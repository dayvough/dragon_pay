# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Dummy::Application.config.secret_key_base = 'c3b242c18a8451478be00aa5f1d661bcf32dbae98122b605edf446667e849685d713c4eac1bd4b491e7ba090260c2485a7ed3cc48a5e26037e0704b138c53608'
