# DragonPay
[![Code Climate](https://codeclimate.com/github/kennethcalamay/dragon_pay.png)](https://codeclimate.com/github/kennethcalamay/dragon_pay)

DragonPay is a payment gateway for interfacing with Philippine banks. To learn more about it, please go to http://dragonpay.ph. This gem is a Ruby Wrapper around their API.

### How to use
1. Declare it in your Gemfile:
```
gem 'dragon_pay'
```

2. Bundle it
```
bundle install
```

3. Set your merchant id and secret key using an initializer:
```ruby
# config/initializers/dragon_pay.rb
DragonPay.configure do |config|
        config.test_mode   = true
        config.merchant_id = '<Your Merchant ID>'
        config.secret_key  = '<Your Secret Key>'
end
```

4. Then from your controller, use the client to generate the url to where you should redirect the user come payment time:
```ruby
client = DragonPay::Client.new(
        transaction_id: '<your reference id for this transaction>',
        customer_email_address: '<email>',
        amount: 24.99)
gateway_url = client.generate_request_url
redirect_to gateway_url
```

### TODO:
1. More usage docs
2. Test/Specs
3. How to contribute docs

### Contributors:
1. [Kenneth Calamay](https://github.com/kennethcalamay), [Appsource, Inc.](http://appsource.biz)
2. [David Marquez](https://github.com/dayvough), [Shirt.ly](http://shirt.ly)
3. Feel free to add your name here ^_^
