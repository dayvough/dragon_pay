$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "dragon_pay/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "dragon_pay"
  s.version     = DragonPay::VERSION
  s.authors     = ["Reynan Albaredo", "Kenneth Lindsey Calamay", "David Marquez"]
  s.email       = ["ralbaredo@appsource.biz", "rubygems@kennethcalamay.com", "david.marquez@me.com"]
  s.homepage    = "https://github.com/kennethcalamay/dragon_pay"
  s.summary     = "Ruby wrapper for DragonPay's API"
  s.description = ""
  s.license     = "MIT License"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", ">= 4.0.1"

  s.add_development_dependency "sqlite3"
  s.add_development_dependency "rspec-rails"
end
