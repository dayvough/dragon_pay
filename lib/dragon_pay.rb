module DragonPay

  autoload :Configuration, 'dragon_pay/configuration'
  autoload :CurrencyCode,  'dragon_pay/currency_code'
  autoload :Client,        'dragon_pay/client'

  class << self
    attr_accessor :configuration
  end

  def self.configuration
    @configuration ||= Configuration.new
  end

  def self.configure
    yield(configuration)
  end

end
