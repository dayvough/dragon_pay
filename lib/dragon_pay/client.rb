require 'digest/sha1'
require 'net/http'

class DragonPay::Client

  # attributes that can be given default value
  attr_accessor :merchant_id, :secret_key, :currency, :digest, :param1, :param2

  # attributes that can be generated from other attributes
  attr_accessor :payment_description

  # attributes that are required
  attr_accessor :transaction_id, :amount, :customer_email_address

  def initialize(options)
    self.transaction_id         = options[:transaction_id]
    self.amount                 = options[:amount]
    self.customer_email_address = options[:customer_email_address]
    fill_in_defaults(options)
  end

  def generate_request_url
    %Q{
      #{payment_url}?
       merchantid = #{merchant_id}&
            txnid = #{transaction_id}&
           amount = #{amount}&
              ccy = #{currency}&
      description = #{uri_encode(payment_description)}&
            email = #{uri_encode(customer_email_address)}&
           digest = #{digest}&
           param1 = #{param1}&
           param2 = #{param2}
    }.split.join
  end

  def inquire_status
    urlString = %Q{
      #{request_url}?
                op = GETSTATUS&
        merchantid = #{DragonPay.configuration.merchant_id}&
       merchantpwd = #{DragonPay.configuration.secret_key}&
             txnid = #{transaction_id}
    }.split.join

    url = URI.parse(urlString)
    req = Net::HTTP::Get.new(url.to_s)
    res = Net::HTTP.start(url.host, url.port) {|http|
      http.request(req)
    }
    res.body
  end


  private

    def payment_url
      "#{DragonPay.configuration.domain}/Pay.aspx"
    end

    def request_url
      "#{DragonPay.configuration.domain}/MerchantRequest.aspx"
    end

    # fields that can be assigned default values
    def fill_in_defaults(options)
      self.merchant_id = options[:merchant_id] || DragonPay.configuration.merchant_id
      self.secret_key  = options[:secret_key]  || DragonPay.configuration.secret_key
      self.currency    = options[:currency]    || DragonPay.configuration.currency
      self.param1      = options[:param1]      || ''
      self.param2      = options[:param2]      || ''

      description = generate_payment_description unless options[:payment_description]
      self.payment_description = uri_encode(description)
    end

    # generates description based on param values
    def generate_payment_description
      "Payment of #{currency}#{amount} from buyer #{customer_email_address} to merchant #{merchant_id}"
    end

    # calculates the sha1 digest of all the parameters
    def digest
      Digest::SHA1.hexdigest(digestable_parameters)
    end

    # combines all the parameters into 1 so that it can be SHA1 digested
    def digestable_parameters
      %Q{
        #{merchant_id}:
        #{transaction_id}:
        #{amount}:
        #{currency}:
        #{payment_description}:
        #{customer_email_address}:
        #{secret_key}
      }.split.join
    end
    
    def amount
      "%.2f" % @amount
    end

    def uri_encode(string)
      CGI::escape(string)
    end

end
