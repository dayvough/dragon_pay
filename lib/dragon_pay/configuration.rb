class DragonPay::Configuration

  attr_accessor :test_mode, :merchant_id, :secret_key, :currency

  def initialize
    @test_mode   = true
    @merchant_id = 'APPSOURCE'
    @secret_key  = 'Jy$mV8qL'
    @currency    = 'PHP'
  end

  def domain
    test_mode ? 'http://test.dragonpay.ph' : 'https://gw.dragonpay.ph'
  end

end
